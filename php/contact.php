<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    require 'PHPMailer/src/Exception.php';
    require 'PHPMailer/src/PHPMailer.php';
    require 'PHPMailer/src/SMTP.php';
    require '../config/PRIVATE/contact.php';
   
    $lang_folder = $_GET['contact-language']=="en"?"/pages/en/":"/pages/fr/";
    if(!filter_var($_GET['contact-email'], FILTER_VALIDATE_EMAIL) || strlen($_GET['contact-message'])==0 || strlen($_GET['contact-name'])==0 || $_GET['contact-email-2'] != ""){
        header("Location: ".$lang_folder."err/contact.html");
        die();
    }
    
    $receiver_email = $smtp_to_email;

    $message = "Contact: ".$_GET['contact-name']."\nEmail: ".$_GET['contact-email']."\nMessage:\n".$_GET['contact-message'];
    
    $mail = new PHPMailer(true);
    
    try{
    	$mail->isSMTP();
    	$mail->Host       = $smtp_host;
    	$mail->SMTPAuth   = true;
    	$mail->Username   = $smtp_username;
    	$mail->Password   = $smtp_password;
    	$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    	$mail->Port       = 587;
    	
    	$mail->setFrom($smtp_from_email, 'Mailer');
    	$mail->addAddress($receiver_email);
    	$mail->addReplyTo($_GET['contact-email'], $_GET['contact-name']);
    	
    	$mail->isHTML(false);
    	$mail->Subject = 'Contact TeleQom.org';
    	$mail->Body    = $message;
    	$mail->AltBody = $message;
    	
    	$mail->send();
    	
    	header("Location: ".$lang_folder."confirmation.html");
    }catch (Exception $e) {
        header("Location: ".$lang_folder."err/contact.html");
        die();
    }
    
?>