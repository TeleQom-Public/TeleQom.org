<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/config/PRIVATE/DB.php';
$lang_folder = $_GET['newsletter-language']=="fr"?"/pages/fr/":"/pages/en/";

if(!filter_var($_GET['newsletter-email'], FILTER_VALIDATE_EMAIL) || $_GET['newsletter-email-2'] != "") {
    header('Location: '.$lang_folder.'err/general.html');
    die();
}

$insert_email = $pdo->prepare('INSERT IGNORE INTO newsletter(email,language) VALUES (?,?)');
$insert_email->execute(array($_GET['newsletter-email'],$_GET['newsletter-language']));
if($insert_email->rowCount()==1){
    header('Location: '.$lang_folder.'confirmation.html');
    die();
}
header('Location: '.$lang_folder.'err/general.html');

?>
