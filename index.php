<?php
    $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    echo($language);
    $accept = ['fr', 'en'];
    $language = in_array($language, $accept) ? $language : 'fr';
    header("location:/pages/{$language}/index.html");
?>